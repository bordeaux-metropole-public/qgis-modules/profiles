import configparser
import json
import logging
import os
import re
import shutil
import tempfile
from pathlib import Path

import requests

# Dossier qui contient les profiles à copier (sur le serveur APP)
repoPath = Path(r"\\APP\APP\MET\QGISBM\qgis3\profiles")

http_proxy  = "http://bm-proxy.bordeaux-it.fr:8080"
https_proxy = "https://bm-proxy.bordeaux-it.fr:8080"

proxyDict = { 
              "http"  : http_proxy, 
              "https" : https_proxy
            }



def getCDFilename(cd):
    """
    Get filename from content-disposition
    """
    if not cd:
        return None
    fname = re.findall("filename=(.+)", cd)
    if len(fname) == 0:
        return None
    return fname[0]


def getPluginVersion(path):
    metadataPath = path / "metadata.txt"
    if metadataPath.exists():
        for line in metadataPath.open():
            for result in re.findall("version=(.*)", line):
                if len(result) > 0:
                    return result
    else:
        logging.warning(str(metadataPath) + " not found")
        return 0


def getProfileVersion(path):
    profilePath = path / "profil.json"
    if profilePath.exists():
        profile = json.load(profilePath.open())
        return profile["version"]
    else:
        logging.warning(str(path) + "/profil.json not found")
        return 0


def ignore_ini(src):
    ignore = []
    for file in src.iterdir():
        if file.suffix == ".ini":
            logging.info("Add to ignore :" + str(file.name))
            ignore.append(file.name)
    return ignore


def overwriteDir(src, dest, ignore=None):
    if src.is_dir():
        if not dest.is_dir():
            os.makedirs(dest)
        if ignore is not None:
            ignored = ignore(src)
        else:
            ignored = set()
        for f in src.iterdir():
            if f.name not in ignored:
                overwriteDir(f, dest / f.name, ignore)
    else:
        shutil.copyfile(src, dest)


def mergeInitFile(profileSrc, profileDest, iniFile):
    src = profileSrc / iniFile
    dest = profileDest / iniFile
    if src.exists():
        if dest.exists():
            configSrc = configparser.ConfigParser(
                interpolation=None, allow_no_value=True, strict=False
            )
            configSrc.optionxform = str
            with open(src, "r") as file:
                data = file.read()
            configSrc.read_string(data)
            configDest = configparser.ConfigParser(
                interpolation=None, allow_no_value=True, strict=False
            )
            configDest.optionxform = str
            configDest.read(dest)
            for section in configSrc:
                if section != "DEFAULT" and section not in configDest.sections():
                    configDest.add_section(section)
                for param in configSrc[section]:
                    configDest[section][param] = configSrc[section][param]
            with dest.open("w") as configfile:
                configDest.write(configfile)
        else:
            shutil.copyfile(src, dest)


def installPlugin(conf, path):
    if conf["type"] == "local":
        pluginPath = Path(conf["url"])
        shutil.copytree(pluginPath, path / conf["name"])
    elif conf["type"] == "remote":
        # Téléchargement et copie du plugin
        logging.info("...... Download plugin")
        r = requests.get(conf["url"], verify=False, allow_redirects=True, proxies=proxyDict)
        # On récupere le nom du fichier depuis les headers
        filename = getCDFilename(r.headers.get("content-disposition"))
        if filename:
            # On stocke l'archive dans un répertoire temporaire
            tempDir = tempfile.mkdtemp()
            tempPath = Path(tempDir) / filename
            tempPath.open("wb").write(r.content)

            # Dézippage du plugin dans le profile
            logging.info("...... Unzip plugin")
            shutil.unpack_archive(tempPath, path)

            # Nettoyage des fichiers temporaires
            logging.info("...... Clean")
            shutil.rmtree(tempDir)
        else:
            logging.error("...... Download error : " + str(r.content))


def addPlugins(conf, profilePath):
    pathToPlugin = "python/plugins"
    # On iter sur les plugins du profil
    for plugin in conf["plugins"]:
        logging.info("plugin : " + plugin["name"])
        pluginPath = profilePath / pathToPlugin / plugin["name"]
        logging.info("path : " + str(pluginPath))
        # On check si le plugin existe
        if pluginPath.exists():
            # Si il existe, on test la version
            pluginVersion = getPluginVersion(pluginPath)
            # Si on a pas la meme version on le remplace par la version du dépot
            if pluginVersion != plugin["version"]:
                logging.info("...... Delete and install plugin")
                shutil.rmtree(pluginPath)
                installPlugin(plugin, profilePath / pathToPlugin)
            else:
                logging.info("plugin up to date : " + pluginVersion)
        else:
            logging.info("...... Install plugin")
            installPlugin(plugin, profilePath / pathToPlugin)


def main():

    path = os.path.expandvars("%APPDATA%/QGIS/QGIS3/profiles")

    if repoPath.exists() and repoPath.is_dir():

        for profileRepoPath in [x for x in repoPath.iterdir() if x.is_dir()]:
            configFile = repoPath / profileRepoPath / "profil.json"
            if configFile.exists():
                try:
                    profile = json.load(
                        open(
                            configFile,
                        )
                    )

                    profilePath = Path(path) / profile["name"]
                    profileUrl = repoPath / profileRepoPath

                    # On check si le profil existe
                    if profilePath.exists():
                        # Si il existe, on test la version
                        profileVersion = getProfileVersion(profilePath)
                        # Si on a pas la meme version on overide les fichiers avec ceux du dépot...
                        if profileVersion != profile["version"]:
                            # On override le profile
                            overwriteDir(profileUrl, profilePath, ignore_ini)

                            # On merge les fichier .ini
                            qgisIniPath = Path("QGIS/QGIS3.ini")
                            qgisCustomisationIniPath = Path("QGIS/QGISCUSTOMIZATION3.ini")
                            mergeInitFile(profileUrl, profilePath, qgisIniPath)
                            mergeInitFile(profileUrl, profilePath, qgisCustomisationIniPath)

                            # On ajoute les plugins
                            addPlugins(profile, profilePath)
                    else:
                    # Si le profil n'existe pas on le copie depuis le dépot
                        shutil.copytree(profileUrl, profilePath)
                    # On ajoute les plugins
                        addPlugins(profile, profilePath)
                except:
                   logging.info("...... Profile access denied") 

if __name__ == "__main__":
    main()
